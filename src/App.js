import React from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  state = {
    snake: [[5, 5], [5, 6]],
    price: [1, 1],
    end: false
  };
  table = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]];

  generatePrice = () => {
    return [Math.floor(Math.random() * 10), Math.floor(Math.random() * 10)];
  };

  moveSnake = (event) => {
    const { snake, price } = this.state;
    let head;
    switch (event.keyCode) {
      case 38: {
        head = [snake[0][0], snake[0][1] - 1];
        break;
      }
      case 39: {
        head = [snake[0][0] + 1, snake[0][1]];
        break;
      }
      case 37: {
        head = [snake[0][0] - 1, snake[0][1]];
        break;
      }
      case 40: {
        head = [snake[0][0], snake[0][1] + 1];
        break;
      }
    }
    if (head) {
      if (head[0] >= 10 || head[1] >= 10 || head[0] < 0 || head[1] < 0) {
        this.setState({ end: true });
      }
      if (head[0] === price[0] && head[1] === price[1]) {
        this.setState({ price: this.generatePrice() });
        this.setState({ snake: [head].concat(snake) });
      } else {
        this.setState({ snake: [head].concat(snake.slice(0, snake.length - 1)) });

      }
    }
  };

  componentDidMount() {
    document.addEventListener("keydown", this.moveSnake, false);
  }

  render() {
    const { snake, price, end } = this.state;
    return (
      <div className="App">
        {end ? (<div className="danger">You Lose!!!!</div>) : this.table.map((row, rowIdx) => (
          <div className="row">
            {row.map((cell, cellIdx) => (<div className="cell">
              {snake.some(el => (el[0] === rowIdx && el[1] === cellIdx)) ? '!!!' : '???'}
              {(price[0] === rowIdx && price[1] === cellIdx) && '******'}
            </div>))}
          </div>
        ))}
      </div>
    );
  }


}

export default App;
